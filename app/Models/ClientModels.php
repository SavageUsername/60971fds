<?php namespace App\Models;
use CodeIgniter\Model;
class ClientModels extends Model
{
    protected $table = 'client'; //таблица, связанная с моделью
    protected $allowedFields = ['id','fio'];
    public function getClient($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getClientWithId($id = null)
    {
        $builder = $this->select('*')->join('bill','client.id = bill.id_client');
        if (!is_null($id))
        {
            return $builder->where(['bill.sum' => $id])->first();
        }
        return $builder;
    }

    public function getBillWithId($id = null, $search = '')
    {
        $builder = $this->select('*')->join('bill','client.id = bill.id_client')->like('fio', $search,'both', null, true);
        if (!is_null($id))
        {
            return $builder->where(['bill.sum' => $id])->first();
        }
        return $builder;
    }
}