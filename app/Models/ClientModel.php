<?php namespace App\Models;
use CodeIgniter\Model;
class ClientModel extends Model
{
    protected $table = 'client'; //таблица, связанная с моделью
    public function getRating($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getRatingWithUser($id = null)
    {
        $builder = $this->select('*')->join('bill','client.id = bill.id_client');
        if (!is_null($id))
        {
            return $builder->where(['bill.sum' => $id])->first();
        }
        return $builder;
    }
}