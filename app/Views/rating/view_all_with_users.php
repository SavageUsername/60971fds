<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
    <?php if (!empty($client) && is_array($client)) : ?>

    <h2>Все клиенты:</h2>
        <div class="d-flex mb-2">
            <?= $pager->links('group1','my_page') ?>
            <?= form_open('rating/viewAllWithUsers', ['style' => 'display: flex']); ?>
            <select name="per_page" class="ml-3" aria-label="per_page">
                <option value="2"  <?php if($per_page == '2') echo("selected"); ?>>2</option>
                <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
                <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
                <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
            </select>
            <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
            </form>
            <?= form_open('rating/viewAllWithUsers',['style' => 'display: flex']); ?>
            <input type="text" class="form-control ml-3" name="search" placeholder="Имя клиента" aria-label="Search"
                   value="<?= $search; ?>">
            <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
            </form>
        </div>
        <table class="table table-striped">
            <thead>
            <th scope="col">Номер клиента</th>
            <th scope="col">ФИО</th>
            <th scope="col">Сумма счета</th>
            <th scope="col">Управление</th>

            </thead>
            <tbody>
            <?php foreach ($client as $item): ?>
                <tr>
                    <td><?= esc($item['id']); ?></td>
                    <td><?= esc($item['fio']); ?></td>
                    <td><?= esc($item['sum']); ?></td>
                    <td>
                        <a href="<?= base_url()?>/rating/view/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                        <a href="<?= base_url()?>/rating/edit/<?= esc($item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                        <a href="<?= base_url()?>/rating/delete/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else : ?>
        <div class="text-center">
            <p>Клиенты не найдены </p>

        </div>
    <?php endif ?>


    </div>
<?= $this->endSection() ?>