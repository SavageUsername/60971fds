<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php use CodeIgniter\I18n\Time; ?>
        <?php if (!empty($client)) : ?>
            <div class="card mb-3" style="max-width: 640px; margin-left: auto; margin-right: auto">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 align="center" class="card-title"><?= esc($client['fio']); ?></h5>
                        </div>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <p>Клиент не найден.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>