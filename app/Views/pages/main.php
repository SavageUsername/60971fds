<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="jumbotron text-center">

    <h1 class="display-4">Фондовая биржа</h1>
    <p class="lead">Это приложение содержит сведения о финансовых инструментах, брокерских счетах и сделках.</p>
    <a class="btn btn-primary btn-lg" href="/auth/login" role="button">Войти</a>
</div>
<?= $this->endSection() ?>
