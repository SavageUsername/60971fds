<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('ClientController/store'); ?>
        <div class="form-group">
            <label for="name">ФИО</label>
            <input type="text" class="form-control <?= ($validation->hasError('fio')) ? 'is-invalid' : ''; ?>" name="fio"
                   value="<?= old('fio'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('fio') ?>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Создать</button>
        </div>
        </form>


    </div>
<?= $this->endSection() ?>