<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php use CodeIgniter\I18n\Time; ?>
        <?php if (!empty($client)) : ?>
            <table class="table table-striped">
                <thead>
                <th scope="col">Номер клиента</th>
                <th scope="col">ФИО</th>
                <th scope="col">Управление</th>

                </thead>
                <tbody>
                <?php foreach ($client as $item): ?>
                    <tr>
                        <td><?= esc($item['id']); ?></td>
                        <td><?= esc($item['fio']); ?></td>
                        <td>
                            <a href="<?= base_url()?>/ClientController/view/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                            <a href="<?= base_url()?>/ClientController/edit/<?= esc($item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                            <a href="<?= base_url()?>/ClientController/delete/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php else : ?>
            <p>Клиент не найден.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>