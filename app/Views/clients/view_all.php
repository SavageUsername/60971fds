<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2>Все клиенты:</h2>

        <?php if (!empty($client) && is_array($client)) : ?>
            <?php foreach ($client as $item): ?>
                <div class="card mb-3" style="max-width: 540px; margin-left: auto; margin-right: auto">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title"><?= esc($item['fio']); ?></h5>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else : ?>
            <p>Невозможно найти клиентов.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>