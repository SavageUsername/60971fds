<?php
namespace App\Controllers;

use App\Models\ClientModel;

class Rating extends BaseController


{
    public function index() //Обображение всех записей
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        if ($this->ionAuth->isAdmin())
        {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;
            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;
            helper(['form','url']);
            $model = new ClientModel();
            $data['client'] = $model->getRatingWithUser()->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            echo view('rating/view_all_with_users', $this->withIon($data));
        }
        else
        {
            session()->setFlashdata('message', lang('Curating.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
    }

    public function view($id = null) //отображение одной записи
    {

        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new ClientModel();
        $data ['client'] = $model->getRating($id);
        echo view('rating/view', $this->withIon($data));
    }

    public function viewAllWithUsers()
    {
        if ($this->ionAuth->isAdmin())
        {
            $model = new ClientModel();
            $data['client'] = $model->getRatingWithUser()->paginate(2, 'group1');
            $data['pager'] = $model->pager;
            echo view('rating/view_all_with_users', $this->withIon($data));
        }
        else
        {
            session()->setFlashdata('message', lang('Curating.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
    }

}