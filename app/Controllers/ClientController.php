<?php namespace App\Controllers;

use App\Models\ClientModels;

class ClientController extends BaseController
{
    public function index() //Обображение всех записей
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new ClientModels();
        $data ['client'] = $model->getClient();
        echo view('clients/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new ClientModels();
        $data ['client'] = $model->getClient($id);
        echo view('clients/view', $this->withIon($data));
    }


    public function viewAllWithUsers()
    {

        if ($this->ionAuth->isAdmin())
        {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;
            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;
            helper(['form','url']);
            $model = new ClientModels();
            $data['client'] = $model->getBillWithId()->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            echo view('clients/view_client', $this->withIon($data));
        }
        else
        {
            session()->setFlashdata('message', lang('Curating.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }

    }
    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([

                'fio' => 'required|min_length[3]|max_length[255]'
            ]))
        {
            $model = new ClientModels();
            $model->save([

                'fio' => $this->request->getPost('fio'),
            ]);
            session()->setFlashdata('message', lang('Curating.rating_create_success'));
            return redirect()->to('/ClientController/viewAllWithUsers');
        }
        else
        {
            return redirect()->to('/ClientController/create')->withInput();
        }
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('clients/create', $this->withIon($data));
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new ClientModels();

        helper(['form']);
        $data ['client'] = $model->getClient($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('clients/edit', $this->withIon($data));

    }

    public function update()
    {
        helper(['form','url']);
        echo '/clients/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'fio' => 'required|min_length[3]|max_length[255]',

            ]))
        {
            $model = new ClientModels();
            $model->save([
                'id' => $this->request->getPost('id'),
                'fio' => $this->request->getPost('fio'),

            ]);
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));

            return redirect()->to('/ClientController/view');
        }
        else
        {
            return redirect()->to('/ClientController/edit/'.$this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new ClientModels();
        $model->delete($id);
        return redirect()->to('/ClientController/view');
    }
}