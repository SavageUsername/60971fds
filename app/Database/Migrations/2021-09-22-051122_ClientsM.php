<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ClientsM extends Migration
{
	public function up()
	{
        if (!$this->db->tableexists('clients'))
        {
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'fio' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));

            // create table
            $this->forge->createtable('clients', TRUE);
        }
	}

	public function down()
	{
        $this->forge->droptable('clients');
	}
}
