<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class T extends Migration
{
    public function up()
    {
        if (!$this->db->tableexists('client'))
        {
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'fio' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));

            // create table
            $this->forge->createtable('client', TRUE);
        }
        if (!$this->db->tableexists('bill'))
        {
            $this->forge->addfield(array(
                'id_client' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'sum' => array('type' => 'INT', 'constraint' => '255', 'null' => FALSE),
                'date' => array('type' => 'DATE', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('id_client','client','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('bill', TRUE);
        }

    }

    public function down()
    {
        $this->forge->droptable('client');
        $this->forge->droptable('bill');
    }
}
