<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class clients_seed extends Seeder
{
    public function run()
    {
        $data = [

            'fio' => 'Client1FromSeed',
        ];
        $this->db->table('client')->insert($data);

        $data = [

            'fio' => 'Client2FromSeed',
        ];
        $this->db->table('client')->insert($data);

        $data = [
            'id_client' => 1,
            'sum' => '15000',
            'date' => '2021-01-30',
        ];
        $this->db->table('bill')->insert($data);


    }
}