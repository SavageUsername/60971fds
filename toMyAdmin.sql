

create database fond3;
USE fond3;
CREATE TABLE client (
  id int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID клиента',
  fio text NOT NULL COMMENT 'ФИО клиента',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 12
AVG_ROW_LENGTH = 1638
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица клиентов'
ROW_FORMAT = DYNAMIC;


CREATE TABLE tiker (
  id int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID тикера',
  name text NOT NULL COMMENT 'Наименование тикера',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 11
AVG_ROW_LENGTH = 1638
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица тикеров'
ROW_FORMAT = DYNAMIC;


CREATE TABLE bill (
  id_client int(11) NOT NULL COMMENT 'ID клиента',
  sum int(11) NOT NULL COMMENT 'Сумма счета',
  date date NOT NULL COMMENT 'Дата время',
  INDEX id_client (id_client),
  CONSTRAINT bill_ibfk_1 FOREIGN KEY (id_client)
  REFERENCES client (id) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
AVG_ROW_LENGTH = 1489
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица счетов клиентов'
ROW_FORMAT = DYNAMIC;


CREATE TABLE deal (
  id int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID сделки',
  id_tiker int(11) NOT NULL COMMENT 'ID тикера',
  id_client int(11) NOT NULL COMMENT 'ID клиента',
  type text NOT NULL COMMENT 'Тип сделки',
  quantity int(10) UNSIGNED NOT NULL COMMENT 'Количество',
  price int(11) NOT NULL COMMENT 'Цена',
  date date NOT NULL COMMENT 'Дата время',
  PRIMARY KEY (id),
  INDEX id_client (id_client),
  INDEX id_tiker (id_tiker),
  CONSTRAINT deal_ibfk_1 FOREIGN KEY (id_client)
  REFERENCES client (id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT deal_ibfk_2 FOREIGN KEY (id_tiker)
  REFERENCES tiker (id) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
AUTO_INCREMENT = 11
AVG_ROW_LENGTH = 1638
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица сделок'
ROW_FORMAT = DYNAMIC;


CREATE TABLE request (
  id int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID заявки',
  id_tiker int(11) NOT NULL COMMENT 'ID тикера',
  id_client int(11) NOT NULL COMMENT 'ID клиента',
  type text NOT NULL COMMENT 'Тип заявки',
  quantity int(10) UNSIGNED NOT NULL COMMENT 'Количество',
  price int(11) NOT NULL COMMENT 'Цена',
  date date NOT NULL COMMENT 'Дата время',
  PRIMARY KEY (id),
  INDEX id_client (id_client),
  INDEX id_tiker (id_tiker),
  CONSTRAINT request_ibfk_1 FOREIGN KEY (id_client)
  REFERENCES client (id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT request_ibfk_2 FOREIGN KEY (id_tiker)
  REFERENCES tiker (id) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
AUTO_INCREMENT = 11
AVG_ROW_LENGTH = 1638
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'Таблица заявок'
ROW_FORMAT = DYNAMIC;

INSERT INTO client VALUES
(1, 'Большаков Евгений Валерьянович'),
(2, 'Горбунов Владимир Николаевич'),
(3, 'Василенко Баста Янукович'),
(4, 'Старых Сергей Владимирович'),
(5, 'Илон Маск Тесла'),
(6, 'Черный Владислав Рюрикович'),
(7, 'Пушкарев Алексей Петрович'),
(8, 'Гросу Адриан'),
(9, 'Лянтин Илья'),
(10, 'Филатов Даниил'),
(11, 'Ниценко Максим');

INSERT INTO tiker VALUES
(1, 'A1'),
(2, 'A2'),
(3, 'A3'),
(4, 'T1'),
(5, 'T2'),
(6, 'T3'),
(7, 'E1'),
(8, 'E2'),
(9, 'D1'),
(10, 'D2');

INSERT INTO bill VALUES
(1, 500000, '2020-03-10'),
(2, 1500000, '2020-03-10'),
(3, 49000, '2019-03-21'),
(4, 550000, '2018-07-22'),
(5, 34000, '2017-10-22'),
(6, 545634, '2017-10-22'),
(7, 643634, '2015-06-06'),
(8, 7000, '2019-02-07'),
(9, 8000, '2019-03-04'),
(10, 59999, '2019-11-03'),
(11, 404323, '2019-08-26');





INSERT INTO deal VALUES
(1, 3, 1, 'покупка', 10, 5006, '2020-01-01'),
(2, 6, 5, 'продажа', 22, 5040, '2020-01-01'),
(3, 5, 3, 'покупка', 13, 5020, '2020-01-01'),
(4, 6, 5, 'покупка', 13, 50540, '2020-01-01'),
(5, 8, 7, 'продажа', 18, 5006, '2020-01-01'),
(6, 9, 8, 'покупка', 13, 5500, '2020-01-01'),
(7, 2, 2, 'покупка', 1, 5040, '2020-01-01'),
(8, 3, 2, 'покупка', 13, 5300, '2020-01-01'),
(9, 4, 5, 'продажа', 2, 2500, '2020-01-01'),
(10, 5, 3, 'покупка', 7, 1500, '2020-01-01');


INSERT INTO request VALUES
(1, 1, 1, 'продажа', 15, 50, '2020-01-01');




